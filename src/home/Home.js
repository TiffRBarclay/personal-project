import React from 'react'
import './Home.css'

function Home({handleActive}) {
    return (
        <div className="home" id="home">
            <div className="container">
                <div className="home-info">
                    <div className="home-title">
                    <h1><mark>Tiffany Barclay</mark></h1>
                    <h2> - Web Developer</h2>
                    </div>
                    <h3>
                        Freelance designer based in Auckland.
                        <br/> 
                        Working hard to bring clients thoughtful and purposeful websites.
                    </h3>
                    <div className="button-container">
                        <a href="#portfolio" className="scroll-button" onClick={() => handleActive('portfolio')} >
                            <span>
                                View Portfolio
                            </span>
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default Home
