import React, { useEffect, useState } from 'react'
import './Header.css'

function Header({active, handleActive}) {
    // const [active, setActive] = useState('home')
    const [backgroundColor, setBackgroundColor] = useState('transparent')

    const listenScrollEvent = () => {
        if(window.scrollY >= 60) {
            setBackgroundColor('#333');
        } else {
            setBackgroundColor('transparent')
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', listenScrollEvent)
    }, [])

    return (
        <div className="header" style={{ backgroundColor }}>
            <div className="nav-links">
                <a href="#" className={`nav-item ${active === 'home' ? 'active' : ''}`} onClick={() => handleActive('home')}>Home</a>
                <a href="#portfolio" className={`nav-item ${active === 'portfolio' ? 'active' : ''}`} onClick={() => handleActive('portfolio')}>Portfolio</a>
            </div>
        </div>
    )
}

export default Header
