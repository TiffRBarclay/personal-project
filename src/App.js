import React, { useState } from 'react';
import './App.css'

import Header from './Header/Header';
import Home from './home/Home';
import Portfolio from './Portfolio/Portfolio';
// import Services from './Services/Services';

function App() {
  const [active, setActive] = useState('home');

  const handleActive = (section) => {
    setActive(section)
  }

  return (
    <div className="App">
      <Header handleActive={handleActive} active={active}/>
      <Home handleActive={handleActive}/>
      {/* <Services /> */}
      <Portfolio />
    </div>
  );
}

export default App;
